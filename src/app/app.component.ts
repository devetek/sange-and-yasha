import { AuthService } from '@providers/services/auth.service';
import { Component } from '@angular/core';
import { Platform, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '@pages/home/home.page';
import { SignInPage } from '@pages/sign-in/sign-in.page';
import { TranslateService } from '@ngx-translate/core';

import { UNAUTHORIZED } from '@constants/events';
import { Device } from '@ionic-native/device';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { ErrorHelper } from '@providers/helpers/error.helper';
import { HttpClient } from '@angular/common/http';
import { API } from '@settings/api';
import { SingleResponse } from '@shared/contracts/server-responses.contract';

@Component({
    templateUrl: 'app.component.html',
})
export class HompesMobile {
    rootPage: any;

    public deviceView: any;

    pesan: string = '';

    constructor(
        private authService: AuthService,
        private events: Events,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public translate: TranslateService,
        private errorHelper: ErrorHelper,
        private http: HttpClient,
        private push: Push,
        public device: Device,
        public alertCtrl: AlertController,
    ) {

        this.deviceView = device;

        platform.ready().then(() => {
            this.setPush();
        });
    }

    ngOnInit() {
        this.initializeApp();
    }

    async initializeApp() {
        await this.platform.ready();

        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.translate.setDefaultLang('en');
        this.translate.use('en');

        await this.setRoute();

    }

    setPush() {
        console.log('mulai------------');
        const options: PushOptions = {
            android: {
                senderID: '984943890172',

            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false',
            },
            // windows: {},
            // browser: {
            //     pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            // }
        };

        const pushObject: PushObject = this.push.init(options);

        // pushObject.on('notification').subscribe((notification: any) => {
        //     console.log('Received a notification', notification);
        //     this.pesan = "berhasil di terima"
        // });

        // pushObject.on('registration').subscribe((registration: any) => {
        //     this.pesan = "Device registered";
        //     console.log('Device registered', registration)
        // });

        // pushObject.on('error').subscribe(error => {
        //     console.error('Error with Push plugin', error)
        //     this.pesan = "Device error";
        // });

        pushObject.on('registration').subscribe(async (data: any) => {
            console.log('device token -> ' + data.registrationId);
            // TODO - send device token to server
            this.authService.token_fcm.next(data.registrationId);

            const isAuthenticated = await this.authService.checkIsAuthenticated();
            console.log(isAuthenticated)
            if (isAuthenticated) {
                this.http
                    .get(`${API.url}/api/v1/firebase_pesan/` + data)
                    .subscribe(
                        data => {
                            console.log('sds')
                        }
                    )
            }
        });

        pushObject.on('notification').subscribe((data: any) => {
            console.log('message -> ' + data.message);
            // if user using app and push notification comes
            if (data.additionalData.foreground) {
                // if application open, show popup
                const confirmAlert = this.alertCtrl.create({
                    title: 'New Notification',
                    message: data.message,
                    buttons: [{
                        text: 'Ignore',
                        role: 'cancel',
                    }, {
                        text: 'View',
                        handler: () => {
                            // TODO: Your logic here
                            // this.nav.push(DetailsPage, { message: data.message });
                            console.log('asdsa isi pesan');
                        },
                    }],
                });
                confirmAlert.present();
            } else {
                // if user NOT using app and push notification comes
                // TODO: Your logic on click of push notification directly
                // this.nav.push(DetailsPage, { message: data.message });
                console.log('Push notification clicked');
            }
        });

        pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
    }

    listenEvents() {
        this.events.subscribe(UNAUTHORIZED, async () => {
            await this.authService.removeCredentials();

            this.rootPage = SignInPage;
        });
    }

    async setRoute() {
        try {
            const isAuthenticated = await this.authService.checkIsAuthenticated();

            if (isAuthenticated) {
                console.log('logi...')
                let fcm = "";
                this.authService.getTokenFcm().subscribe(
                    data => {
                        console.log(data);
                        this.http
                            .get(`${API.url}/api/v1/firebase_pesan/` + data)
                            .subscribe(
                                data => {
                                    console.log('sds')
                                }
                            )

                    }

                )

                // kirim ka server 

                this.rootPage = HomePage;




                return;
            }

            this.rootPage = SignInPage;
        } catch (err) {
            this.rootPage = SignInPage;
        }
    }
}
