import { Push } from '@ionic-native/push';

import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

// Services, plugins, helpers
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

// Components
import { MyPacket } from '@components/my-packet/my-packet.component';
import { Packet } from '@components/packet/packet.component';

// Pages
import { HompesMobile } from './app.component';
import { Header } from '@components/header/header.component';
import { Sidebar } from '@components/sidebar/sidebar.component';
import { BookmarkedPage } from '@pages/bookmarked/bookmarked';
import { CategoryPage } from '@pages/category/category';
import { CourseDetailsPage } from '@pages/course-details/course-details';
import { CoursePage } from '@pages/course/course';
import { DesignPage } from '@pages/design/design';
import { FiltersPage } from '@pages/filters/filters';
import { HomePage } from '@pages/home/home.page';
import { LecturerProfilePage } from '@pages/lecturer-profile/lecturer-profile';
import { MyProfilePage } from '@pages/my-profile/my-profile';
import { NotificationPage } from '@pages/notification/notification';
import { OngoingCoursesPage } from '@pages/ongoing-courses/ongoing-courses';
import { SearchPage } from '@pages/search/search';
import { SignUpPage } from '@pages/sign-up/sign-up';
import { SignInPage } from '@pages/sign-in/sign-in.page';
import { SupportPage } from '@pages/support/support';
import { SubscribePage } from '@pages/subscribe/subscribe.page';
import {
    UnpaidSubscriptionListPage,
} from '@pages/unpaid-subscription-list/unpaid-subscription-list.page';
import { PaymentConfirmationPage } from '@pages/payment-confirmation/payment-confirmation.page';
import { TicketThreadPage } from '@pages/ticket-thread/ticket-thread.page';
import { TicketSubmissionPage } from '@pages/ticket-submission/ticket-submission.page';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '@providers/user/user';
import { IonicStorageModule } from '@ionic/storage';
import { PacketProvider } from '@providers/packet/packet';
import { PacketCategoryProvider } from '@providers/packet-category/packet-category';
import { HttpErrorInterceptor } from '@providers/http-error-interceptor/http-error-interceptor';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { MyPacketProvider } from '@providers/my-packet/my-packet';
import { MyCourseDetails } from '@pages/my-course-details/my-course-details';
import { PacketRequirementProvider } from '@providers/packet-requirement/packet-requirement';
import { PacketDetailsProvider } from '@providers/packet-details/packet-details';
import { PacketGoalProvider } from '@providers/packet-goal/packet-goal';
import { CourseReadingPage } from '@pages/course-reading/course-reading';
import { BookContentTicketProvider } from '@providers/book-content-ticket/book-content-ticket';

import { AuthService } from '@providers/services/auth.service';
import { BookService } from '@providers/services/book.service';
import { BookContentService } from '@providers/services/book-content.service';
import { PurchaseService } from '@providers/services/purchase.service';
import { TicketService } from '@providers/services/ticket.service';

import { ErrorHelper } from '@providers/helpers/error.helper';

import { FileSourceConverterPipe } from './pipes/file-source-converter.pipe';
import { Device } from '@ionic-native/device';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
        // Components
    Header,
    Sidebar,
    MyPacket,
    Packet,

        // Pages
    HompesMobile,
    BookmarkedPage,
    CategoryPage,
    CourseDetailsPage,
    CoursePage,
    DesignPage,
    FiltersPage,
    HomePage,
    LecturerProfilePage,
    MyProfilePage,
    NotificationPage,
    OngoingCoursesPage,
    SearchPage,
    SignUpPage,
    SignInPage,
    SupportPage,
    MyCourseDetails,
    CourseReadingPage,
    SubscribePage,
    UnpaidSubscriptionListPage,
    PaymentConfirmationPage,
    TicketThreadPage,
    TicketSubmissionPage,

        // Pipes
    FileSourceConverterPipe,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(HompesMobile, {
        preloadModules: true,
      }),
    HttpClientModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient],
          },
      }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
        // Components
    MyPacket,

        // Pages
    HompesMobile,
    BookmarkedPage,
    CategoryPage,
    CourseDetailsPage,
    CoursePage,
    DesignPage,
    FiltersPage,
    HomePage,
    LecturerProfilePage,
    MyProfilePage,
    NotificationPage,
    OngoingCoursesPage,
    SearchPage,
    SignUpPage,
    SignInPage,
    SupportPage,
    MyCourseDetails,
    CourseReadingPage,
    SubscribePage,
    UnpaidSubscriptionListPage,
    PaymentConfirmationPage,
    TicketThreadPage,
    TicketSubmissionPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    UserProvider,
    PacketProvider,
    PacketCategoryProvider,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    PacketBookProvider,
    MyPacketProvider,
    PacketRequirementProvider,
    PacketDetailsProvider,
    PacketGoalProvider,
    BookContentTicketProvider,
        // Services
    AuthService,
    BookService,
    BookContentService,
    PurchaseService,
    TicketService,
        // Helpers,
    ErrorHelper,
    Device,
    Push,
  ],
})
export class AppModule {
  static injector: Injector;

  constructor(injector: Injector) {
    AppModule.injector = injector;
  }
}
