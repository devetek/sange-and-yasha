import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertFileSource',
  pure: true,
})
export class FileSourceConverterPipe implements PipeTransform {
  private window: any = window;

  public transform(value: string): string {
    return this.window.Ionic.WebView.convertFileSrc(value);
  }
}
