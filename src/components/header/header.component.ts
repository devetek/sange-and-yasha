import { Component, OnInit } from '@angular/core';
import { App } from 'ionic-angular';

import { AuthService } from '@providers/services/auth.service';
import { UserProvider } from '@providers/user/user';
import { User } from '@models/user';

import { MyProfilePage } from '@pages/my-profile/my-profile';

@Component({
  selector: 'header',
  templateUrl: 'header.component.html',
})
export class Header implements OnInit {
  currentUser: User;

  constructor(
    private authService: AuthService,
    private appCtrl: App,
    private userProv: UserProvider,
  ) { }

  public ngOnInit() {
    this.getAuthorization();
  }

  private async getAuthorization() {
    const isAuthenticated = await this.authService.checkIsAuthenticated();

    if (isAuthenticated) {
      this.userProv.getUser().subscribe(value => this.currentUser = value);
    }
  }

  public goToMyProfilePage() {
    this.appCtrl.getRootNav().push(MyProfilePage);
  }
}
