import { Component, Input } from '@angular/core';
import { MyPacket as Model } from '@models/my-packet';

@Component({
  selector: 'my-packet',
  templateUrl: 'my-packet.component.html',
})
export class MyPacket {
  @Input() packet: Model;
}
