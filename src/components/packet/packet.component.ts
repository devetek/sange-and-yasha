import { Component, Input } from '@angular/core';
import { Packet as Model } from '@models/packet';

@Component({
  selector: 'packet',
  templateUrl: 'packet.component.html',
})
export class Packet {
  @Input() packet: Model;
}
