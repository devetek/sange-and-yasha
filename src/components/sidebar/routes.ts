import { HomePage } from '@pages/home/home.page';
import { OngoingCoursesPage } from '@pages/ongoing-courses/ongoing-courses';
import { BookmarkedPage } from '@pages/bookmarked/bookmarked';
import { SupportPage } from '@pages/support/support';
import { NotificationPage } from '@pages/notification/notification';
import {
    UnpaidSubscriptionListPage,
} from '@pages/unpaid-subscription-list/unpaid-subscription-list.page';

const pages = [
    { title: 'home', component: HomePage },
    { title: 'ongoing_courses', component: OngoingCoursesPage },
    { title: 'unpaid_subscription', component: UnpaidSubscriptionListPage },
    //   { title: 'bookmarked', component: BookmarkedPage },
    { title: 'support', component: SupportPage },
    { title: 'notifications', component: NotificationPage },
];

export { pages };
