import { App, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { AuthService } from '@providers/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { SignInPage } from '@pages/sign-in/sign-in.page';
import { ERROR_MESSAGE } from '@constants/errors';

import { pages } from './routes';

@Component({
  selector: 'sidebar',
  templateUrl: 'sidebar.component.html',
})
export class Sidebar {
  pages: { title: string, component: any }[];

  constructor(
    private alertCtrl: AlertController,
    private authProvider: AuthService,
    private appCtrl: App,
    public translateService: TranslateService,
  ) {
    this.pages = pages;
  }

  public openPage(page: any) {
    this.appCtrl.getRootNav().push(page.component);
  }

  public async logOut() {
    try {
      await this.authProvider.removeCredentials();

      this.appCtrl.getRootNav().push(SignInPage);
    } catch (error) {
      const message = ERROR_MESSAGE(
        await this.translateService.get('unexpected_error_occured').toPromise(),
      );
      const alert = this.alertCtrl.create(message);

      alert.present();
    }
  }
}
