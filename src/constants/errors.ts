const LOADING_MESSAGE = (content = 'Please wait...') => ({ content });

const ERROR_MESSAGE = (message = 'Unexpected error') => ({
  message,
  title: 'Error',
  buttons: ['Ok'],
});

export { LOADING_MESSAGE, ERROR_MESSAGE };
