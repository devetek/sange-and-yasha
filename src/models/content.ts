export interface Content {
  id_book_content: number;
  id_book_section: number;
  order_book_content: number;
  time_book_content: number;
  title_book_content: string;
  book_content_type: string;
  book_content: string;
  validasi: number;
  id_user: number;
  created_at: string;
  updated_at: string;
}
