export interface PacketDetails {
  id_packet_detail: number;
  id_packet: number;
  detail_packet: string;
  icon: string;
  icon_mobile: string;
  order_detail: number;
  updated_at: string;
  created_at: string;
}
