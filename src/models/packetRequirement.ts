export interface PacketRequirement {
  id_packet_requirements: number;
  id_packet: number;
  order_requirement: number;
  icon: string;
  icon_mobile: string;
  validasi: number;
  requirement: string;
  created_at: string;
  updated_at: string;
}
