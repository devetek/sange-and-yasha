export interface TicketAnswer {
  answer: string;
  created_at: string;
  date_answer: string;
  has_approved: string;
  id_ticket: number;
  id_ticket_answer: number;
  id_user: number;
  name: string;
  updated_at: string;
}
