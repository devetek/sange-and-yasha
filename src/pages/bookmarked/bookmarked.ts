import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CourseDetailsPage } from '@pages/course-details/course-details';
@Component({
  selector: 'page-bookmarked',
  templateUrl: 'bookmarked.html',
})
export class BookmarkedPage {
  constructor(public navCtrl: NavController) { }

  courcedetails() {
    this.navCtrl.push(CourseDetailsPage);
  }
}
