import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, NavParams } from 'ionic-angular';

import { CourseDetailsPage } from '@pages/course-details/course-details';
import { FiltersPage } from '@pages/filters/filters';
import { PacketProvider } from '@providers/packet/packet';
import { Packet } from '@models/packet';

@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  id_packet_category_master: number;
  name_category: string;
  loading: any;
  dataPackets: Packet[] = [];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public packetProv: PacketProvider,
    private loadingCtrl: LoadingController,
    public navParam: NavParams,
  ) {
    this.id_packet_category_master = navParam.get('id_packet_category_master');
    this.name_category = navParam.get('name_category');
    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
  }

  filters() {
    const modal = this.modalCtrl.create(FiltersPage);
    modal.present();
  }

  goToCourseDetailsPage(id) {
    this.navCtrl.push(CourseDetailsPage, { id_packet: id });
  }

  ionViewDidLoad() {
    this.getPacketByCategory();
  }

  getPacketByCategory() {
    this.loading.present();
    this.packetProv.get({ id_packet_category_master: this.id_packet_category_master })
      .subscribe((value) => {
        this.dataPackets = value.data;

        this.loading.dismiss();
      });
  }
}
