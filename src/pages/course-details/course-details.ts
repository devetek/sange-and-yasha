import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  Loading,
  LoadingController,
  AlertController,
  Alert,
} from 'ionic-angular';

import { LecturerProfilePage } from '@pages/lecturer-profile/lecturer-profile';
import { SubscribePage } from '@pages/subscribe/subscribe.page';

import { Packet } from '@models/packet';
import { PacketGoal } from '@models/packet-goal';
import { PacketDetails } from '@models/packet-details';
import { PacketRequirement } from '@models/packetRequirement';
import { PacketProvider } from '@providers/packet/packet';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { PacketBook } from '@models/packet-book';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';
import { Observable } from 'rxjs';

@Component({
  selector: 'course-details-page',
  templateUrl: 'course-details.html',
})
export class CourseDetailsPage {
  tab = 'about';

  id_packet: number;
  dataPacket: Packet;
  dataGoal: PacketGoal;
  dataDetail: PacketDetails;
  dataRequirement: PacketRequirement;
  dataBooks: PacketBook[] = [];
  loading: Loading;
  alert: Alert;

  constructor(
    public navCtrl: NavController,
    public packetProv: PacketProvider,
    public packetBookProv: PacketBookProvider,
    public navParam: NavParams,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
  ) {
    this.id_packet = navParam.get('id_packet');
  }

  ionViewDidLoad() {
    this.getData();
  }

  private getData() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    Observable.zip(
      this.packetProv.detail(this.id_packet),
      this.packetBookProv.get({ id_packet: this.id_packet }),
    ).subscribe(
      ([packet, books]) => {
        this.dataPacket = packet.data.packet;
        this.dataDetail = packet.data.packet_detail;
        this.dataGoal = packet.data.packet_goal;
        this.dataRequirement = packet.data.packet_requirement;

        this.dataBooks = books.data;

        this.loading.dismiss();
      },
      (error) => {
        this.alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));
        this.alert.present();
      },
    );
  }

  getDetail() {
    this.packetProv.detail(this.id_packet)
      .subscribe((value) => {
        this.dataPacket = value.data.packet;
        this.dataDetail = value.data.packet_detail;
        this.dataGoal = value.data.packet_goal;
        this.dataRequirement = value.data.packet_requirement;
      });
  }

  getListBooks() {
    this.packetBookProv.get({ id_packet: this.id_packet })
      .subscribe((value) => {
        this.dataBooks = value.data;
      });
  }

  goToLecturerProfilePage() {
    this.navCtrl.push(LecturerProfilePage);
  }

  goToSubscribePage() {
    this.navCtrl.push(SubscribePage, {
      packetId: this.dataPacket.id_packet,
    });
  }
}
