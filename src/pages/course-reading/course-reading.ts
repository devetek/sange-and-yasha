import { Component } from '@angular/core';
import {
  LoadingController,
  NavController,
  NavParams,
  Loading,
  AlertController,
} from 'ionic-angular';
import { Observable } from 'rxjs';

import { BookContentService } from '@providers/services/book-content.service';
import { TicketService } from '@providers/services/ticket.service';
import { BookContent } from '@models/book';
import { Ticket } from '@models/ticket';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';
import { TicketThreadPage } from '@pages/ticket-thread/ticket-thread.page';
import { TicketSubmissionPage } from '@pages/ticket-submission/ticket-submission.page';

@Component({
  selector: 'page-course',
  templateUrl: 'course-reading.html',
})
export class CourseReadingPage {
  tab = 'lactures';
  id_book_content: number;
  id_packet: number;

  loading: Loading;
  dataContent: BookContent;
  dataTicket: Ticket[] = [];

  constructor(
    private navCtrl: NavController,
    private navParam: NavParams,
    private alertCtrl: AlertController,
    private bookService: BookContentService,
    private ticketService: TicketService,
    private loadingCtrl: LoadingController,
  ) {
    this.id_book_content = parseInt(this.navParam.get('id_book_content'), 10) || 0;
    this.id_packet = parseInt(this.navParam.get('id_packet'), 10) || 0;
  }

  ionViewDidLoad() {
    this.fetchDataHandler();
  }

  fetchDataHandler() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    Observable.zip(
      this.bookService.find(this.id_book_content),
      this.ticketService.get(this.id_book_content),
    ).subscribe(
      ([book, tickets]) => {
        this.dataContent = book.data;
        this.dataTicket = tickets.data;
        this.loading.dismiss();
      },
      (error) => {
        const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

        this.loading.dismiss();
        alert.present();
      },
    );
  }

  goToTicketThreadPage(ticket) {
    this.navCtrl.push(TicketThreadPage, { ticket });
  }

  goToTicketSubmissionPage() {
    this.navCtrl.push(TicketSubmissionPage, {
      book_content: this.dataContent,
      id_packet: this.id_packet,
    });
  }
}
