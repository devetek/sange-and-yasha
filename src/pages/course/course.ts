import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { BookService } from '@providers/services/book.service';
import { Book, Section } from '@models/book';
import { CourseReadingPage } from '@pages/course-reading/course-reading';

@Component({
  selector: 'page-course',
  templateUrl: 'course.html',
})
export class CoursePage {
  tab = 'lactures';
  id_book: number;
  id_packet: number;
  loading: any;
  dataBook: Book;
  dataSection: Section[] = [];

  constructor(
    public navCtrl: NavController,
    public navParam: NavParams,
    public bookService: BookService,
    private loadingCtrl: LoadingController,
  ) {
    this.id_book = parseInt(navParam.get('id_book'), 10) || 0;
    this.id_packet = parseInt(navParam.get('id_packet'), 10) || 0;
    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.getBooks();
  }

  getBooks() {
    this.bookService.get(this.id_book).subscribe(
      (value: { data: { book: Book; sections: Section[]; }; }) => {
        this.dataBook = value.data.book;
        this.dataSection = value.data.sections;
        this.loading.dismiss();
      },
    );
  }

  startReading(id: any) {
    this.navCtrl.push(CourseReadingPage, {
      id_book_content: id,
      id_packet: this.id_packet,
    });
  }
}
