import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CategoryPage } from '@pages/category/category';
@Component({
  selector: 'page-design',
  templateUrl: 'design.html',
})
export class DesignPage {
  constructor(public navCtrl: NavController) { }

  category() {
    this.navCtrl.push(CategoryPage);
  }
}
