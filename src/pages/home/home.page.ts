import { Component } from '@angular/core';
import {
  App,
  NavController,
  ModalController,
  LoadingController,
  AlertController,
  Loading,
} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { MyPacket } from '@models/my-packet';
import { PacketCategory, PacketCategoryProvider } from '@providers/packet-category/packet-category';
import { MyPacketProvider } from '@providers/my-packet/my-packet';

import { SearchPage } from '@pages/search/search';
import { CategoryPage } from '@pages/category/category';
import {
  UnpaidSubscriptionListPage,
} from '@pages/unpaid-subscription-list/unpaid-subscription-list.page';
import { OngoingCoursesPage } from '@pages/ongoing-courses/ongoing-courses';
import { MyCourseDetails } from '@pages/my-course-details/my-course-details';

import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';

/**
 * TODO: Split home page section
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.page.html',
})
export class HomePage {
  public packetCategories: PacketCategory[];
  public myPackets: MyPacket[];

  private loading: Loading;

  constructor(
    private alertCtrl: AlertController,
    private appCtrl: App,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private myPacketProv: MyPacketProvider,
    private navCtrl: NavController,
    private packageCategoryProv: PacketCategoryProvider,
    private translateService: TranslateService,
  ) { }

  public async ngOnInit() {
    await this.dataFetchHandler();
  }

  private async dataFetchHandler() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    try {
      const getCategories = this.packageCategoryProv.get().toPromise();
      const getMyPackets = this.myPacketProv.get({ limit: 2 }).toPromise();

      this.packetCategories = (await getCategories).data;
      this.myPackets = (await getMyPackets).data;

      this.loading.dismiss();
    } catch (error) {
      const message = ERROR_MESSAGE(
        await this.translateService.get('network_error_occured').toPromise(),
      );
      const alert = this.alertCtrl.create(message);

      this.loading.dismiss();
      alert.present();
    }
  }

  public onClickSearchHandler() {
    const modal = this.modalCtrl.create(SearchPage, { param: this.packetCategories });

    modal.present();
  }

  public goToCategoryPage(id: number, name_category: string) {
    this.appCtrl.getRootNav().push(CategoryPage, {
      name_category,
      id_packet_category_master: id,
    });
  }

  public goToNotificationPage() {
    this.appCtrl.getRootNav().push(UnpaidSubscriptionListPage);
  }

  public goToOngoingCoursesPage() {
    this.navCtrl.push(OngoingCoursesPage);
  }

  public goToMyCourseDetails(id: number) {
    this.appCtrl.getRootNav().push(MyCourseDetails, { id_packet: id });
  }
}
