import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CourseDetailsPage } from '@pages/course-details/course-details';
@Component({
  selector: 'page-lecturerprofile',
  templateUrl: 'lecturer-profile.html',
})
export class LecturerProfilePage {
  tab = 'about';

  constructor(public navCtrl: NavController) { }

  courcedetails() {
    this.navCtrl.push(CourseDetailsPage);
  }
}
