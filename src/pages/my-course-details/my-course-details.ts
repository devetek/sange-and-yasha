import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';

import { LecturerProfilePage } from '@pages/lecturer-profile/lecturer-profile';
import { PacketGoal } from '@models/packet-goal';
import { PacketDetails } from '@models/packet-details';
import { PacketRequirement } from '@models/packetRequirement';
import { PacketProvider } from '@providers/packet/packet';
import { PacketBookProvider } from '@providers/packet-book/packet-book';
import { PacketBook } from '@models/packet-book';
import { MyPacket } from '@models/my-packet';
import { PacketGoalProvider } from '@providers/packet-goal/packet-goal';
import { PacketDetailsProvider } from '@providers/packet-details/packet-details';
import { PacketRequirementProvider } from '@providers/packet-requirement/packet-requirement';
import { CoursePage } from '@pages/course/course';

@Component({
  selector: 'course-details-page',
  templateUrl: 'my-course-details.html',
})
export class MyCourseDetails {
  tab = 'videos';

  id_packet: number;
  dataPacket: MyPacket;
  dataGoal: PacketGoal[];
  dataDetail: PacketDetails[];
  dataRequirement: PacketRequirement[];
  dataBooks: PacketBook[] = [];

  loading: any;

  constructor(
    public navCtrl: NavController,
    public packetProv: PacketProvider,
    public packetDetailProv: PacketDetailsProvider,
    public packetGoalProv: PacketGoalProvider,
    public packetRequirementProv: PacketRequirementProvider,
    public packetBookProv: PacketBookProvider,
    private loadingCtrl: LoadingController,
    public navParam: NavParams,
  ) {
    this.id_packet = parseInt(navParam.get('id_packet'), 10);

    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.getPacket();
    this.getListBooks();
    this.getGoals();
    this.getRequirement();
    this.getDetail();
  }

  getPacket() {
    this.packetProv.mydetail(this.id_packet)
      .subscribe((value) => {
        this.dataPacket = value.data;
      });
  }

  getDetail() {
    this.packetDetailProv.get({ id_packet: this.id_packet })
      .subscribe((value) => {
        this.dataDetail = value.data;
      });
  }

  getGoals() {
    this.packetGoalProv.get({ id_packet: this.id_packet })
      .subscribe((value) => {
        this.dataGoal = value.data;
      });
  }

  getRequirement() {
    this.packetRequirementProv.get({ id_packet: this.id_packet })
      .subscribe((value) => {
        this.dataRequirement = value.data;
      });
  }

  getListBooks() {
    this.packetBookProv.get({ id_packet: this.id_packet })
      .subscribe((value) => {
        this.dataBooks = value.data;
        this.loading.dismiss();
      });
  }

  lecturerprofile() {
    this.navCtrl.push(LecturerProfilePage);
  }

  toBookDetail(id) {
    this.navCtrl.push(CoursePage, {
      id_book: id,
      id_packet: this.id_packet,
    });
  }
}
