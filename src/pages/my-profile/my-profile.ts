import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { UserProvider } from '@providers/user/user';
import { User } from '@models/user';

@Component({
  selector: 'page-myprofile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
  loading: any;

  currentUser: User = {
    id: 0,
    name: '',
    email: '',
    email_verified_at: '',
    created_at: '',
    updated_at: '',
    id_division: '',
    id_from_division: '',
    id_group: '',
  };

  constructor(
    public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public userProv: UserProvider
    ,
  ) {
    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
  }

  ionViewDidLoad() { }

  ngOnInit(): void {
    this.loading.present();
    this.getMember();
  }

  getMember() {
    this.userProv.getUser()
      .subscribe((value) => {
        this.currentUser = value;
        this.loading.dismiss();
      });
  }

  update() {
    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    this.loading.present();
    this.loading.dismiss();
  }
}
