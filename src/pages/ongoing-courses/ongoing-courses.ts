import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';

import { MyPacketProvider } from '@providers/my-packet/my-packet';
import { MyPacket } from '@models/my-packet';
import { MyCourseDetails } from '@pages/my-course-details/my-course-details';

@Component({
  selector: 'page-ongoingcourses',
  templateUrl: 'ongoing-courses.html',
})
export class OngoingCoursesPage {
  dataMyPacket: MyPacket[] = [];
  loading: any;

  constructor(
    public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public myPacketProv: MyPacketProvider,
  ) {
    this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.getListMyPacket();
  }

  goToMyCourseDetailsPage(id) {
    this.navCtrl.push(MyCourseDetails, { id_packet: id });
  }

  getListMyPacket() {
    this.myPacketProv.get({})
      .subscribe((value) => {
        this.dataMyPacket = value.data;
        this.loading.dismiss();
      });
  }
}
