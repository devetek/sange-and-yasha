import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Loading,
  Alert,
} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File, FileEntry } from '@ionic-native/file';

import { Packet } from '@models/packet';
import { PacketProvider } from '@providers/packet/packet';
import { PurchaseService } from '@providers/services/purchase.service';
import { UnpaidPurchase } from '@shared/contracts/unpaid-purchase.contract';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';

import { HomePage } from '@pages/home/home.page';

@Component({
  selector: 'payment-confirmation-page',
  templateUrl: 'payment-confirmation.page.html',
})
export class PaymentConfirmationPage {
  public purchase: UnpaidPurchase;
  public packet: Packet;
  public loading: Loading;

  private cameraOptions: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    mediaType: this.camera.MediaType.PICTURE,
  };

  public form: any = {
    transferred_form_channel: '',
    transferred_form_name: '',
    amount_paid: '',
    file: {
      uri: '',
      data: null,
    },
  };

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private camera: Camera,
    private file: File,
    private navCtrl: NavController,
    private navParams: NavParams,
    private packetProvider: PacketProvider,
    private purchaseService: PurchaseService,
  ) { }

  public ngOnInit() {
    this.purchase = this.navParams.get('purchase');
    this.getDetails(this.purchase.id_packet);
  }

  public get purchaseTotal(): number {
    return this.packet
      ? this.packet.price + 0 + this.purchase.unique_code
      : 0;
  }

  private getDetails(packetId: number) {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.packetProvider.detail(packetId)
      .subscribe(
        (value) => {
          this.packet = value.data.packet;
          this.loading.dismiss();
        },
        (error) => {
          const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }

  public async handlePickImage() {
    this.form.file.uri = await this.camera.getPicture(this.cameraOptions);
    this.form.file.data = await this.prepareProofOfPayment(this.form.file.uri);
  }

  private prepareProofOfPayment(path: string) {
    return new Promise((resolve, reject) => {
      this.file.resolveLocalFilesystemUrl(path).then((fileEntry: FileEntry) => {
        fileEntry.file((file) => {
          const reader = new FileReader();

          reader.onloadend = (event: any) => {
            const blob = new Blob([event.target.result], {
              type: file.type,
            });

            resolve(blob);
          };

          reader.readAsArrayBuffer(file);
        });
      });
    });
  }

  public handlePaymentConfirmation() {
    let alert: Alert;

    // Validation
    if (
      !this.form.transferred_form_channel ||
      !this.form.transferred_form_name ||
      !this.form.amount_paid ||
      !this.form.file
    ) {
      alert = this.alertCtrl.create(ERROR_MESSAGE('Please fill the missing fields'));

      alert.present();
      return;
    }

    if (parseFloat(this.form.amount_paid) < this.purchaseTotal) {
      alert = this.alertCtrl.create(ERROR_MESSAGE('Not enough payment amount'));

      alert.present();
      return;
    }

    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.purchaseService.confirmPayment(this.purchase.id_purchase, this.form)
      .subscribe(
        (value) => {
          this.packet = value.data.packet;
          this.loading.dismiss();

          this.navCtrl.setRoot(HomePage);
        },
        (error) => {
          alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }
}
