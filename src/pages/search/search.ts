import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

import { PacketProvider } from '@providers/packet/packet';
import { Packet } from '@models/packet';

import { DesignPage } from '@pages/design/design';
import { CourseDetailsPage } from '@pages/course-details/course-details';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchInput = '';
  packetList: Packet[];

  constructor(
        private navCtrl: NavController,
        private viewCtrl: ViewController,
        private packetProv: PacketProvider,
    ) { }

  ngOnInit() {
    this.getPacket();
  }

  getItems(event: any) {
    if (event.key === 'Enter' && this.searchInput.length > 3) {
            // TODO: Search flow do here
            // tslint:disable-next-line: no-console
        console.log('=== |data input and keyCode| ===', {
            keyCode: event.key,
            searchInput: this.searchInput,
          });
      }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  design() {
    this.navCtrl.push(DesignPage);
  }

  getPacket() {
    this.packetProv.get({})
            .subscribe((value) => {
              this.packetList = value.data;
            });
  }
  goToCourseDetailsPage(id: number) {
    this.navCtrl.push(CourseDetailsPage, { id_packet: id });

  }
}
