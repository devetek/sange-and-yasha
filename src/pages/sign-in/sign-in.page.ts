import { Component, HostListener } from '@angular/core';
import {
    AlertController,
    LoadingController,
    MenuController,
    NavController,
    NavParams,
} from 'ionic-angular';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

import { AuthService } from '@providers/services/auth.service';
import { HomePage } from '@pages/home/home.page';
import { SignUpPage } from '@pages/sign-up/sign-up';
import { Push } from '@ionic-native/push';
import { API } from '@settings/api';
import { SingleResponse } from '@shared/contracts/server-responses.contract';

@Component({
    selector: 'sign-in-page',
    templateUrl: 'sign-in.page.html',
})
export class SignInPage {
    loading: any;
    formLogin: any = {
        email: '',
        password: '',
    };
    globalEnterPress = 0;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private menuCtrl: MenuController,
        private http: HttpClient,
        private push: Push,
        private authService: AuthService,
    ) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
    }

    ionViewDidLoad() {
        this.checkAuthenticated();
    }

    async checkAuthenticated() {
        try {
            const isAuthenticated = await this.authService.checkIsAuthenticated();
            if (isAuthenticated) {


                this.authService.getTokenFcm().subscribe(
                    data => {

                        this.http
                            .get(`${API.url}/api/v1/firebase_pesan/` + data)
                            .subscribe(
                                data => {
                                    console.log('sds')
                                }
                            )
                    }

                )

                this.menuCtrl.enable(true);
                this.navCtrl.setRoot(HomePage);
            }
        } catch (err) {
            const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });
            const errMessage = 'System error occurred. Please try again later';

            alert.setMessage(errMessage);
            alert.present();
        }
    }

    @HostListener('document:keypress', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            this.globalEnterPress += 1;

            if (this.globalEnterPress <= 1) {

                const validating = this.validateLoginData(this.formLogin);

                if (validating.isValid) {
                    this.doLogin(this.formLogin);
                } else {
                    const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });

                    alert.setMessage(validating.message);
                    alert.present();
                }
            }
        }
    }

    submitForm(data: any) {
        const validating = this.validateLoginData(this.formLogin);

        if (validating.isValid) {
            this.doLogin(data);
        } else {
            const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });

            alert.setMessage(validating.message);
            alert.present();
        }
    }

    async doLogin(data: any) {
        this.loading = this.loadingCtrl.create({ content: 'Please wait ...' });
        this.loading.present();

        try {
            const response = await this.authService.login(data);
            await this.authService.storeCredentials(response);
            this.loading.dismiss();

            await this.checkAuthenticated();
        } catch (err) {
            this.loading.dismiss();

            const alert = this.alertCtrl.create({ title: 'Error', buttons: ['Ok'] });
            let errMessage = 'System error occurred. Please try again later';

            if (err instanceof HttpErrorResponse && err.status === 401) {
                errMessage = err.error.message;
            }

            alert.setMessage(errMessage);
            alert.present();
        }
    }

    validateLoginData(input: any) {
        const REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;

        if (!input.email) {
            return { isValid: false, message: 'Email cannot be empty.' };
        }

        if (!input.password) {
            return { isValid: false, message: 'Password cannot be empty.' };
        }

        if (input.password.length < 6) {
            return { isValid: false, message: 'Password at least 6 characters.' };
        }

        if (!REGEX.test(input.email)) {
            return { isValid: false, message: 'Please enter a valid email address.' };
        }

        return { isValid: true, message: '' };
    }

    home() {
        this.navCtrl.setRoot(HomePage);
    }

    signup() {
        this.navCtrl.setRoot(SignUpPage);
    }
}
