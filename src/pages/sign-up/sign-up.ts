import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '@pages/home/home.page';
import { SignInPage } from '@pages/sign-in/sign-in.page';

@Component({
  selector: 'page-signup',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  constructor(public navCtrl: NavController) { }

  home() {
    this.navCtrl.setRoot(HomePage);
  }

  singin() {
    this.navCtrl.setRoot(SignInPage);
  }
}
