import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Packet } from '@models/packet';
import { PacketProvider } from '@providers/packet/packet';
import { PurchaseService } from '@providers/services/purchase.service';
import { HomePage } from '@pages/home/home.page';

import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';

@Component({
  selector: 'subscribe-page',
  templateUrl: 'subscribe.page.html',
})
export class SubscribePage {
  public packet: Packet;
  public loading: any;

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private navParams: NavParams,
    private packetProvider: PacketProvider,
    private purchaseService: PurchaseService,
  ) { }

  public ngOnInit() {
    this.getDetails(this.navParams.get('packetId'));
  }

  protected getDetails(packetId: number) {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.packetProvider.detail(packetId)
      .subscribe(
        (value) => {
          this.packet = value.data.packet;
          this.loading.dismiss();
        },
        (error) => {
          const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }

  public goToHome() {
    this.navCtrl.push(HomePage);
  }

  public confirmSubscribeHandler() {
    this.loading = this.loadingCtrl.create({ content: 'Processing...' });
    this.loading.present();

    this.purchaseService.purchase(this.packet.id_packet_sale)
      .subscribe(
        () => {
          this.loading.dismiss();
          this.goToHome();
        },
        (error) => {
          const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }
}
