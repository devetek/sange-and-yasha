import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Loading,
  Alert,
} from 'ionic-angular';

import { TicketService } from '@providers/services/ticket.service';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';
import { BookContent } from '@models/book';
import { CourseReadingPage } from '@pages/course-reading/course-reading';

@Component({
  selector: 'ticket-submission-page',
  templateUrl: 'ticket-submission.page.html',
})
export class TicketSubmissionPage {
  public loading: Loading;
  public bookContent: BookContent;
  public id_packet: number;

  public form: any = {
    title_ticket: '',
    ticket_content: '',
    submodule_id: null,
    package_id: null,
  };

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private navParams: NavParams,
    private ticketService: TicketService,
  ) { }

  public ngOnInit() {
    this.bookContent = this.navParams.get('book_content');
    this.form.package_id = parseInt(this.navParams.get('id_packet'), 10) || 0;
    this.form.submodule_id = this.bookContent.id_book_content;
  }

  public handleSubmit() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.ticketService.submitTicket(this.form)
      .subscribe(
        () => {
          this.loading.dismiss();
          this.navCtrl.push(CourseReadingPage, {
            id_book_content: this.bookContent.id_book_content,
            id_packet: this.id_packet,
          });
        },
        (error) => {
          const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }
}
