import { Component } from '@angular/core';
import {
  LoadingController,
  NavController,
  NavParams,
  Loading,
  AlertController,
} from 'ionic-angular';

import { TicketService } from '@providers/services/ticket.service';
import { Ticket } from '@models/ticket';
import { TicketAnswer } from '@models/ticket-answer';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';

@Component({
  selector: 'ticket-thread-page',
  templateUrl: 'ticket-thread.page.html',
})
export class TicketThreadPage {
  ticket_id: number;
  loading: Loading;
  ticket: Ticket;
  answers: TicketAnswer[] = [];

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private navParam: NavParams,
    private ticketService: TicketService,
    private loadingCtrl: LoadingController,
  ) { }

  ionViewDidLoad() {
    this.ticket = this.navParam.get('ticket');
    this.fetchDataHandler();
  }

  fetchDataHandler() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.ticketService.getAnswers(this.ticket.id_ticket)
      .subscribe(
        (res) => {
          this.answers = res.data;
          this.loading.dismiss();
        },
        (error) => {
          const alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));

          this.loading.dismiss();
          alert.present();
        },
      );
  }
}
