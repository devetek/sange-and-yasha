import { Component } from '@angular/core';
import { Loading, LoadingController, AlertController, Alert, NavController } from 'ionic-angular';

import { PurchaseService } from '@providers/services/purchase.service';
import { UnpaidPurchase } from '@shared/contracts/unpaid-purchase.contract';
import { LOADING_MESSAGE, ERROR_MESSAGE } from '@constants/errors';

import {
  PaymentConfirmationPage,
} from '@pages/payment-confirmation/payment-confirmation.page';

@Component({
  selector: 'unpaid-subscription-list-page',
  templateUrl: 'unpaid-subscription-list.page.html',
})
export class UnpaidSubscriptionListPage {
  private loading: Loading;
  private alert: Alert;
  public data: UnpaidPurchase[] = [];

  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private purchaseService: PurchaseService,
  ) { }

  public ionViewDidLoad() {
    this.fetchDataHandler();
  }

  private fetchDataHandler() {
    this.loading = this.loadingCtrl.create(LOADING_MESSAGE());
    this.loading.present();

    this.purchaseService.getUnpaidPurchases()
      .subscribe(
        (data) => {
          this.data = data;
          this.loading.dismiss();
        },
        (error) => {
          this.alert = this.alertCtrl.create(ERROR_MESSAGE(error.message));
          this.alert.present();
        },
      );
  }

  public goToSubscriptionPaymentConfirmationPage(purchase: UnpaidPurchase) {
    this.navCtrl.push(PaymentConfirmationPage, { purchase });
  }
}
