import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { Ticket } from '@models/ticket';

@Injectable()
export class BookContentTicketProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param: any): Observable<ListResponse<Ticket>> {
    return this.http
      .get(`${API.url}/api/v1/ticket/client/tickets`, { params: param })
      .map((value: ListResponse<Ticket>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
