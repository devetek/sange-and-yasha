import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { SingleResponse } from '@shared/contracts/server-responses.contract';
import { BookContent } from '@models/book';
import { API } from '@settings/api';

@Injectable()
export class BookContentProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(book_content_id: any): Observable<SingleResponse<BookContent>> {
    return this.http
      .get(`${API.url}/api/v1/book/client/contents/${book_content_id}`)
      .map((value: SingleResponse<BookContent>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
