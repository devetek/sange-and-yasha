import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

@Injectable()
export class ErrorHelper {
  public handle(error: any) {
    // TODO: Implement error logging
    if (error instanceof HttpErrorResponse) {
      switch (error.status) {
        case 500: return Observable.throw(new Error('Server error'));
      }
    }

    return Observable.throw(error);
  }
}
