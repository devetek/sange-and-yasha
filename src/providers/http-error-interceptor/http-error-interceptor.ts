import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';

import { Observable } from 'rxjs';
import { _throw } from 'rxjs/observable/throw';
import { catchError } from 'rxjs/operators';

import { UNAUTHORIZED } from '@constants/events';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(
    private events: Events,
    private storage: Storage,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const promise = this.storage.get('token');

    return Observable.fromPromise(promise)
      .mergeMap((token) => {
        const clonedReq = this.addToken(request, token);

        return next.handle(clonedReq).pipe(
          catchError((error) => {
            // 401 Unauthorized error
            if (error instanceof HttpErrorResponse && error.status === 401) {
              this.events.publish(UNAUTHORIZED);
            }

            return _throw(error);
          }),
        );
      });
  }

  private addToken(request: HttpRequest<any>, token: any) {
    if (token) {
      let clone: HttpRequest<any>;
      clone = request.clone({
        setHeaders: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      return clone;
    }

    return request;
  }
}
