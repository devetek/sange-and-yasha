import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { SingleResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { Member } from '@models/member';

@Injectable()
export class MemberProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(): Observable<SingleResponse<Member>> {
    return this.http
      .get(`${API.url}/api/v1/member/client/members`)
      .map((value: SingleResponse<Member>) => value)
      .catch(this.errorHelper.handle);
  }

  put(id: number, data: Member): Observable<SingleResponse<Member>> {
    return this.http
      .put(`${API.url}/api/v1/member/client/members/${id.toString()}`, { params: data })
      .map((value: SingleResponse<Member>) => value)
      .catch(this.errorHelper.handle);
  }
}
