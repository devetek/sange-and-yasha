import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { MyPacket } from '@models/my-packet';
import { API } from '@settings/api';

@Injectable()
export class MyPacketProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param): Observable<ListResponse<MyPacket>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/my_packets`, { params: param })
      .map((value: ListResponse<MyPacket>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
