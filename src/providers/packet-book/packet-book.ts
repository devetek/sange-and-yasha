import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { PacketBook } from '@models/packet-book';

@Injectable()
export class PacketBookProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param: any): Observable<ListResponse<PacketBook>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_books`, { params: param })
      .map((value: ListResponse<PacketBook>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }

  detail(id: string): Observable<ListResponse<any>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_books/${id}`)
      .map((value: ListResponse<any>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
