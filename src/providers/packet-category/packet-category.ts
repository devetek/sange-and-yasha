import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

import { AuthService } from '@providers/services/auth.service';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { Storage } from '@ionic/storage';

export interface PacketCategory {
  id_packet_master_category: number;
  name_category: string;
  description_category: string;
  validasi?: any;
  id_company: number;
  id_user?: any;
  created_at: string;
  updated_at?: any;
  cover_category: string;
}

@Injectable()
export class PacketCategoryProvider {
  baseUrl = 'https://jsonplaceholder.typicode.com';
  headers: HttpHeaders;
  options: any;

  constructor(
    public http: HttpClient,
    public storage: Storage,
    public auth: AuthService,
  ) { }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  get(): Observable<ListResponse<PacketCategory>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_categories`)
      .map((value: ListResponse<PacketCategory>) => {
        return value;
      })
      .catch(this.handleError);
  }
}
