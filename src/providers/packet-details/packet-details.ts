import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { PacketDetails } from '@models/packet-details';
import { API } from '@settings/api';

@Injectable()
export class PacketDetailsProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param: any): Observable<ListResponse<PacketDetails>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_details`, { params: param })
      .map((value: ListResponse<PacketDetails>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
