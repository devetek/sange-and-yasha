import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { PacketGoal } from '@models/packet-goal';

@Injectable()
export class PacketGoalProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param: any): Observable<ListResponse<PacketGoal>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_goals`, { params: param })
      .map((value: ListResponse<PacketGoal>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
