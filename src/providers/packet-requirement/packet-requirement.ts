import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse } from '@shared/contracts/server-responses.contract';
import { PacketRequirement } from '@models/packetRequirement';
import { API } from '@settings/api';

@Injectable()
export class PacketRequirementProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param: any): Observable<ListResponse<PacketRequirement>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packet_requirements`, { params: param })
      .map((value: ListResponse<PacketRequirement>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
