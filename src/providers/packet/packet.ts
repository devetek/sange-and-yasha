import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse, SingleResponse } from '@shared/contracts/server-responses.contract';
import { Packet } from '@models/packet';
import { API } from '@settings/api';

@Injectable()
export class PacketProvider {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(param): Observable<ListResponse<Packet>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packets`, { params: param })
      .map((value: ListResponse<Packet>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }

  detail(id): Observable<SingleResponse<any>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/packets/${id}`)
      .map((value: SingleResponse<any>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }

  mydetail(id): Observable<SingleResponse<any>> {
    return this.http
      .get(`${API.url}/api/v1/packet/client/my_packets/${id}`)
      .map((value: SingleResponse<any>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
