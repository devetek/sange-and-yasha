import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { API } from '@settings/api';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthService {
    isAuthenticated = false;

    constructor(
        public http: HttpClient,
        private storage: Storage,
    ) { }

    public async checkIsAuthenticated() {
        const now = Date.now();
        const auth: any = await this.storage.get('auth');

        if (!auth || auth.expired_at <= now) {
            return false;
        }


        return true;
    }

    public async getToken() {
        await this.storage.ready();

        try {
            const data = await this.storage.get('auth');

            return data.access_token || null;
        } catch (error) {
            return;
        }
    }

    public login(user: any) {
        const request = {
            grant_type: 'password',
            client_id: API.passport.client_id,
            client_secret: API.passport.client_secret,
            username: user.email,
            password: user.password,
        };

        return this.http.post(`${API.url}/oauth/token`, request).toPromise();
    }

    public register(user: any) {
        return this.http.post(`${API.url}/api/register`, user).toPromise();
    }

    public async removeCredentials() {
        await this.storage.remove('auth');
    }

    public async storeCredentials(response: any) {
        const expired_at = (response.expires_in * 1000) + Date.now();

        const setAuth = this.storage.set('auth', {
            expired_at,
            access_token: response.access_token,
            refresh_token: response.refresh_token,
        });

        const setToken = this.storage.set('token', response.access_token);

        await setAuth;
        await setToken;
    }

    token_fcm = new BehaviorSubject<string>(null);
    getTokenFcm() {
        return this.token_fcm;
    }

    public setTokenFcm(token: string) {
        this.token_fcm.next(token);
        this.storage.set('token_fcm', token);
    }
}
