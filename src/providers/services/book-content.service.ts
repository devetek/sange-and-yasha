import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { SingleResponse, ListResponse } from '@shared/contracts/server-responses.contract';
import { BookContent } from '@models/book';
import { Ticket } from '@models/ticket';
import { API } from '@settings/api';

@Injectable()
export class BookContentService {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  public find(book_content_id: any): Observable<SingleResponse<BookContent>> {
    return this.http
      .get(`${API.url}/api/v1/book/client/contents/${book_content_id}`)
      .map((value: SingleResponse<BookContent>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
