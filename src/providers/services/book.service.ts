import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { SingleResponse } from '@shared/contracts/server-responses.contract';
import { API } from '@settings/api';
import { BookResponse } from '@models/book';

@Injectable()
export class BookService {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  get(id_book: number): Observable<SingleResponse<BookResponse>> {
    return this.http
      .get(`${API.url}/api/v1/book/client/books/${id_book}`)
      .map((value: SingleResponse<BookResponse>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
