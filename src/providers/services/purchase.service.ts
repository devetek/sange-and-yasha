import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import moment from 'moment';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse, SingleResponse } from '@shared/contracts/server-responses.contract';
import { UnpaidPurchase } from '@shared/contracts/unpaid-purchase.contract';
import { API } from '@settings/api';

@Injectable()
export class PurchaseService {
  constructor(
    public http: HttpClient,
    public errorHelper: ErrorHelper,
  ) { }

  public purchase(salePacketId: number): Observable<SingleResponse<any>> {
    const payload = {
      sale_package_id: salePacketId,
    };

    return this.http
      .post(`${API.url}/api/v1/purchases`, payload)
      .catch(this.errorHelper.handle);
  }

  public getUnpaidPurchases(): Observable<UnpaidPurchase[]> {
    return this.http
      .get(`${API.url}/api/v1/my-purchases/unpaid`)
      .map((response: ListResponse<any>) => {
        const data = response.data.map(item => ({
          ...item,
          date_purchase: moment(item.date_purchase),
        }));

        return data;
      })
      .catch(this.errorHelper.handle);
  }

  public confirmPayment(purchaseId: number, data: any): Observable<SingleResponse<any>> {
    const payload = new FormData();

    payload.append('transferred_form_channel', data.transferred_form_channel);
    payload.append('transferred_form_name', data.transferred_form_name);
    payload.append('amount_paid', data.amount_paid);
    payload.append('file', data.file.data);

    return this.http
      .post(`${API.url}/api/v1/purchases/${purchaseId}/confirm-payment`, payload)
      .catch(this.errorHelper.handle);
  }
}
