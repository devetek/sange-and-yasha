import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ErrorHelper } from '@providers/helpers/error.helper';
import { ListResponse, SingleResponse } from '@shared/contracts/server-responses.contract';
import { Ticket } from '@models/ticket';
import { TicketAnswer } from '@models/ticket-answer';
import { API } from '@settings/api';

@Injectable()
export class TicketService {
  constructor(
    private errorHelper: ErrorHelper,
    private http: HttpClient,
  ) { }

  public get(book_content_id: any): Observable<ListResponse<Ticket>> {
    return this.http
      .get(`${API.url}/api/v1/tickets/${book_content_id}`)
      .map((value: ListResponse<Ticket>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }

  public getAnswers(ticket_id: any): Observable<ListResponse<TicketAnswer>> {
    return this.http
      .get(`${API.url}/api/v1/tickets/${ticket_id}/answers`)
      .map((value: ListResponse<TicketAnswer>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }

  public submitTicket(payload: any): Observable<SingleResponse<any>> {
    return this.http
      .post(`${API.url}/api/v1/tickets`, payload)
      .map((value: SingleResponse<any>) => {
        return value;
      })
      .catch(this.errorHelper.handle);
  }
}
