import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';

import { User } from '@models/user';
import { API } from '@settings/api';

@Injectable()
export class UserProvider {
  constructor(public http: HttpClient, private storage: Storage) { }

  async getUserInfo() {
    const auth: any = await this.storage.get('auth');
    const headers: HttpHeaders = new HttpHeaders({
      Authorization: `Bearer ${auth.access_token}`,
    });

    return this.http.get(`${API.url}/api/user`, { headers }).toPromise();
  }

  getUser(): Observable<User> {
    return this.http.get(`${API.url}/api/user`).map(
      (value: User) => {
        return value;
      },
    );
  }
}
