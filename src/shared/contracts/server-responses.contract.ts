
export interface ListResponse<T> {
  data: T[];
  error: string[];
  message: string;
  success?: boolean;
}

export interface SingleResponse<T> {
  data: T;
  error: string[];
  message: string;
  success?: boolean;
}
