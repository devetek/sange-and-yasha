export interface UnpaidPurchase {
  id_purchase: number;
  date_purchase: Date;
  unique_code: number;
  id_packet: number;
  name_packet: string;
  cover_packet: string;
  thum_packet: string;
  price: number;
  id_packet_sale: number;
  name_company: string;
}
