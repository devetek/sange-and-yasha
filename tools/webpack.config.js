/*
 * The webpack config exports an object that has a valid webpack configuration
 * For each environment name. By default, there are two Ionic environments:
 * "dev" and "prod". As such, the webpack.config.js exports a dictionary object
 * with "keys" for "dev" and "prod", where the value is a valid webpack configuration
 * For details on configuring webpack, see their documentation here
 * https://webpack.js.org/configuration/
 */

const path = require('path');
const webpack = require('webpack');
const ionicWebpackFactory = require(process.env.IONIC_WEBPACK_FACTORY);
const Dotenv = require('dotenv-webpack');

const ModuleConcatPlugin = require('webpack/lib/optimize/ModuleConcatenationPlugin');
const PurifyPlugin = require('@angular-devkit/build-optimizer').PurifyPlugin;
const { pathResolver } = require('./helpers');

const resolverAlias = {
  alias: {
    '@app': pathResolver('./src/app'),
    '@components': pathResolver('./src/components'),
    '@constants': pathResolver('./src/constants'),
    '@assets': pathResolver('./src/assets'),
    '@models': pathResolver('./src/models'),
    '@pages': pathResolver('./src/pages'),
    '@providers': pathResolver('./src/providers'),
    '@settings': pathResolver('./src/settings'),
    '@shared': pathResolver('./src/shared'),
    '@theme': pathResolver('./src/theme'),
  },
  extensions: ['.ts', '.js'],
};

const optimizedProdLoaders = [
  {
    test: /\.json$/,
    loader: 'json-loader',
  },
  {
    test: /\.js$/,
    loader: [
      {
        loader: process.env.IONIC_CACHE_LOADER,
      },

      {
        loader: '@angular-devkit/build-optimizer/webpack-loader',
        options: {
          sourceMap: true,
        },
      },
    ],
  },
  {
    test: /\.ts$/,
    loader: [
      {
        loader: process.env.IONIC_CACHE_LOADER,
      },

      {
        loader: '@angular-devkit/build-optimizer/webpack-loader',
        options: {
          sourceMap: true,
        },
      },

      {
        loader: process.env.IONIC_WEBPACK_LOADER,
      },
    ],
  },
];

function getProdLoaders() {
  if (process.env.IONIC_OPTIMIZE_JS === 'true') {
    return optimizedProdLoaders;
  }
  return devConfig.module.loaders;
}

const devConfig = {
  target: 'web',
  entry: process.env.IONIC_APP_ENTRY_POINT,
  output: {
    path: '{{BUILD}}',
    publicPath: 'build/',
    filename: '[name].js',
    devtoolModuleFilenameTemplate: ionicWebpackFactory.getSourceMapperFunction(),
  },
  devtool: process.env.IONIC_SOURCE_MAP_TYPE,

  resolve: {
    extensions: ['.ts', '.js', '.json'],
    modules: [pathResolver('node_modules')],
    ...resolverAlias,
  },

  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.ts$/,
        loader: process.env.IONIC_WEBPACK_LOADER,
      },
    ],
  },

  plugins: [
    new Dotenv({
      path: pathResolver('config/.env.dev'), // load this now instead of the ones in '.env'
      systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      silent: true, // hide any errors
    }),
    ionicWebpackFactory.getIonicEnvironmentPlugin(),
    ionicWebpackFactory.getCommonChunksPlugin(),
    new webpack.DefinePlugin({
      constants: JSON.stringify({ NODE_ENV: 'development' }),
    }),
  ],

  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};

const prodConfig = {
  target: 'web',
  entry: process.env.IONIC_APP_ENTRY_POINT,
  output: {
    path: '{{BUILD}}',
    publicPath: 'build/',
    filename: '[name].js',
    devtoolModuleFilenameTemplate: ionicWebpackFactory.getSourceMapperFunction(),
  },
  devtool: process.env.IONIC_SOURCE_MAP_TYPE,

  resolve: {
    extensions: ['.ts', '.js', '.json'],
    modules: [pathResolver('node_modules')],
    ...resolverAlias,
  },

  module: {
    loaders: getProdLoaders(),
  },

  plugins: [
    new Dotenv({
      path: pathResolver('config/.env.prod'), // load this now instead of the ones in '.env'
      systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      silent: true, // hide any errors
    }),
    ionicWebpackFactory.getIonicEnvironmentPlugin(),
    ionicWebpackFactory.getCommonChunksPlugin(),
    new ModuleConcatPlugin(),
    new PurifyPlugin(),
    new webpack.DefinePlugin({
      constants: JSON.stringify({ NODE_ENV: 'production' }),
    }),
  ],

  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};

module.exports = {
  dev: devConfig,
  prod: prodConfig,
};
